package tf.bug.dot.tu

trait AsToml[A] {

  def asToml(a: A): String

}

object AsToml {

  def apply[A](a: A)(implicit asToml: AsToml[A]): String = asToml.asToml(a)

  object implicits {

    implicit def listToml[A](implicit aAsToml: AsToml[A]): AsToml[List[A]] = new AsToml[List[A]] {
      override def asToml(a: List[A]): String =
        s"[ ${a.map(e => s"{ ${e.toml.replaceAll("\n", ", ")} }").mkString(", ")} ]"
    }

    implicit def stringToml: AsToml[String] = new AsToml[String] {
      override def asToml(a: String): String = '"' + a + '"'
    }

    implicit class TomlOps[A](c: A)(implicit cAsToml: AsToml[A]) {

      def toml: String = cAsToml.asToml(c)

    }

  }

}
