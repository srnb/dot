package tf.bug.dot.editing

import java.io.File

import tf.bug.dot.Selection

object GList {

  def apply(group: Option[String]): Unit = {
    val f = find
    group match {
      case Some(g) =>
        f.get(g) match {
          case Some(l) =>
            l.sortBy {
              case (name, _) => name
            }.foreach {
              case (name, b) =>
                println(fansi.Str(name).overlay(if (b) fansi.Color.Green else fansi.Color.Red))
            }
          case None =>
            println(
              fansi.Str("Didn't find a group of ").overlay(fansi.Color.Yellow) ++ fansi
                .Str(g)
                .overlay(fansi.Color.LightYellow))
        }
      case None =>
        f.mapValues(l =>
            (l.exists {
              case (_, exists) => exists
            }, l.forall {
              case (_, exists) => exists
            }))
          .toList
          .sortBy {
            case (name, _) => name
          }
          .foreach {
            case (name, (some, all)) =>
              val c = (some, all) match {
                case (false, false) => fansi.Color.Red
                case (true, false) => fansi.Color.Yellow
                case (true, true) => fansi.Color.Green
              }
              println(fansi.Str(name).overlay(c))
          }
    }
  }

  def find: Map[String, List[(String, Boolean)]] = {
    val s = Selection()
    s.groups.map { g =>
      (
        g.name,
        g.entries.map { e =>
          val f = new File(System.getProperty("user.home"), e.home)
          (e.home, f.exists())
        }
      )
    }.toMap
  }

}
