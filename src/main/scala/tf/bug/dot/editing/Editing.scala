package tf.bug.dot.editing

import java.io.File
import java.nio.file.Files

import scopt.OptionParser
import tf.bug.dot.BuildInfo

object Editing {

  val parser: OptionParser[CmdOpts] = new OptionParser[CmdOpts]("dot") {
    head("dot", BuildInfo.version)

    cmd("add")
      .action((_, c) => c.copy(add = true))
      .text("Adds an entry to a group.")
      .children(
        arg[String]("group").action((s, c) => c.copy(group = Some(s))).text("the software group"),
        arg[String]("repo_location")
          .action((s, c) => c.copy(repoloc = Some(s)))
          .text("the file relative to the group directory"),
        arg[File]("local_location")
          .action((s, c) => c.copy(localloc = Some(s)))
          .text("the file relative to the home directory")
      )

    note("")

    cmd("remove")
      .action((_, c) => c.copy(remove = true))
      .text("Removes an entry from a group, or a whole group.")
      .children(
        arg[String]("group").action((s, c) => c.copy(group = Some(s))).text("the software group"),
        arg[String]("repo_location")
          .optional()
          .action((s, c) => c.copy(repoloc = Some(s)))
          .text("the file relative to the group directory")
      )

    note("")

    cmd("list")
      .action((_, c) => c.copy(list = true))
      .text("Lists all groups from config, or all files of a group.")
      .children(
        arg[String]("group").optional().action((s, c) => c.copy(group = Some(s))).text("the software group")
      )

    note("")

    cmd("update")
      .action((_, c) => c.copy(update = true))
      .text("Try to pull the repository from the remote and update any symlinks that need to be updated.")

    note("")

    cmd("default").action((_, c) => c.copy(default = true)).text("Resets the selection config to default.")

    note("")

    cmd("sync")
      .action((_, c) => c.copy(sync = true))
      .text("Point all existing dotfiles to a new file in the repository.")

    note("\nOptions:\n")

    opt[Boolean]('f', "force")
      .action((b, c) => c.copy(force = b))
      .text("do not ask for confirmation when" +
        " resetting config to default or force push to the remote")

    help("help").text("prints this text")

    checkConfig {
      case c if Seq(c.add, c.remove, c.list, c.update, c.default).count(identity) != 1 =>
        Left("One and only one has to be specified of add, remove, update, and default.")
      case _ => Right()
    }

  }

  def jump(a: Seq[String]): Unit = {
    parser.parse(a, CmdOpts()) match {
      case Some(c) if c.add => Add(c.group, c.repoloc, c.localloc)
      case Some(c) if c.remove => // Todo
      case Some(c) if c.list => GList(c.group)
      case Some(c) if c.update => // Todo
      case Some(c) if c.default => // Todo
      case Some(c) if c.sync => // Todo
      case None =>
    }
  }

  def symLink(target: File, name: File): Unit = {
    Files.createSymbolicLink(name.toPath, target.toPath)
  }

  def move(orig: File, newF: File): Unit = {
    Files.move(orig.toPath, newF.toPath)
  }

}
