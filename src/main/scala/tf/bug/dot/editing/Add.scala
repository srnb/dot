package tf.bug.dot.editing

import java.io.File

import tf.bug.dot.{Config, Entry, Group, Selection}

object Add {

  def apply(group: Option[String], repoloc: Option[String], localloc: Option[File]): Unit = {
    (group, repoloc, localloc) match {
      case (Some(g), Some(r), Some(l)) =>
        val config = Config()
        val gdir = new File(config.local, g)
        gdir.mkdirs()
        if (l.exists()) {
          val repolocF = new File(gdir, r)
          println(fansi.Str("Moving original file to dotfiles directory...").overlay(fansi.Color.LightCyan))
          Editing.move(l, repolocF)
          println(fansi.Str("Linking old file to new file...").overlay(fansi.Color.LightCyan))
          Editing.symLink(repolocF, l)
        } else {
          println(fansi.Str("The local file you specified does not exist.").overlay(fansi.Color.Yellow))
        }
        println(fansi.Str("Updating configuration file...").overlay(fansi.Color.LightCyan))
        val s = Selection()
        val groups = s.groups
        val groupo = groups.find(_.name == g).getOrElse(Group(g, Nil))
        val ngroup = groupo.copy(
          entries = Entry(
            new File(System.getProperty("user.home")).toPath.relativize(l.toPath).toString,
            r
          ) :: groupo.entries)
        val ns = s.copy(ngroup :: s.groups.filter(_.name != g))
        Selection.set(ns)
        println(fansi.Str("Configuration and linking updated!").overlay(fansi.Color.LightCyan ++ fansi.Bold.On))
      case _ =>
    }
  }

}
