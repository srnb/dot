package tf.bug.dot.editing

import java.io.File

case class CmdOpts(
  add: Boolean = false,
  remove: Boolean = false,
  list: Boolean = false,
  update: Boolean = false,
  default: Boolean = false,
  sync: Boolean = false,
  force: Boolean = false,
  group: Option[String] = None,
  repoloc: Option[String] = None,
  localloc: Option[File] = None)
