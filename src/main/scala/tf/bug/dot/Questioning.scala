package tf.bug.dot

import fansi.{EscapeAttr, Str}

import scala.annotation.tailrec
import scala.io.StdIn

object Questioning {

  val bold: EscapeAttr = fansi.Bold.On
  val green: EscapeAttr = fansi.Color.Green
  val yellow: EscapeAttr = fansi.Color.Yellow

  def question(name: String, example: String, default: String): Str = {
    fansi.Str(s"$name? Ex: $example").overlay(green) ++ fansi.Str(s" [=$default] ").overlay(green ++ bold)
  }

  def ask(q: String, ex: String, default: String): String = {
    print(Questioning.question(q, ex, default))
    val l = StdIn.readLine()
    if (l.isEmpty) {
      default
    } else {
      l
    }
  }

  @tailrec def validate(
    q: String,
    ex: String,
    default: String,
    validation: String => Boolean,
    error: String): String = {
    val l = Questioning.ask(q, ex, default)
    if (validation(l)) {
      l
    } else {
      println(fansi.Str(error).overlay(yellow))
      Questioning.validate(q, ex, default, validation, error)
    }
  }

  def boolean(q: String): Boolean = {
    print(fansi.Str(q).overlay(green) ++ " [Y/n] ")
    val l = StdIn.readLine()
    if (l.toLowerCase.startsWith("n")) {
      false
    } else {
      true
    }
  }
}
