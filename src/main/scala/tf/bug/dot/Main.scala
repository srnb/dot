package tf.bug.dot

import java.io.{File, FileWriter, IOException}
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{FileVisitResult, Files, Path, SimpleFileVisitor}

import tf.bug.dot.Config.implicits._
import tf.bug.dot.Selection.implicits._
import tf.bug.dot.editing.Editing
import tf.bug.dot.tu.AsToml.implicits._

object Main {

  val art: String =
    """
      |             _                 _
      |            (_)               (_)
      |       _  _ (_)   _  _  _   _ (_) _
      |     _(_)(_)(_) _(_)(_)(_)_(_)(_)(_)
      |    (_)     (_)(_)       (_)  (_)
      |    (_)     (_)(_)       (_)  (_)  _
      |    (_)_  _ (_)(_)_  _  _(_)  (_)_(_)
      |      (_)(_)(_)  (_)(_)(_)      (_)
      |
      |
    """.stripMargin

  def main(args: Array[String]): Unit = {
    val config = new File(System.getProperty("user.home"), ".dot/config.toml")
    if (!config.exists()) {
      printArt()
      val nc = Setup.run()
      dissolveLinks(nc)
      deleteLocal(nc)
      setupRepo(nc)

      println(
        fansi
          .Str("dot should be all set up! Run `dot --help` to see the things you can do with dot!")
          .overlay(fansi.Back.LightGreen))

      config.getParentFile.mkdirs()
      config.createNewFile()
      val cfw = new FileWriter(config)
      cfw.write(nc.toml)
      cfw.close()
    } else {
      Editing.jump(args.toSeq)
    }
  }

  def printArt(): Unit = {
    val coloredArt = art.lines.zipWithIndex.map {
      case (line, y) =>
        line.zipWithIndex.map {
          case (char, x) =>
            fansi.Color.True(0, 127 + (5 * y), 255 - (2 * x))(fansi.Str(Seq(char).mkString))
        }.mkString
    }.mkString("\n")
    println(coloredArt)
  }

  def dissolveLinks(nc: Config): Unit = {
    if (nc.local.listFiles().nonEmpty) {
      if (Questioning.boolean("Dissolve links to dotfiles directory?" +
            " This will make it so that when the content of the directory is deleted your dotfiles will not be erased," +
            " and can be added back easily.")) {
        println(
          fansi.Str("Dissolving links to dotfiles directory...").overlay(fansi.Underlined.On ++ fansi.Color.Yellow))
        Setup.clean(nc.local)
        println(fansi.Str("Done!").overlay(fansi.Underlined.On ++ fansi.Color.Yellow))
      }
    }
  }

  def deleteLocal(nc: Config): Unit = {
    println(fansi.Str("Deleting existing dotfiles directory...").overlay(fansi.Bold.On ++ fansi.Color.Red))
    val directory = nc.local.toPath
    Files.walkFileTree(directory, pureDeletion)
    println(fansi.Str("Complete!").overlay(fansi.Bold.On ++ fansi.Color.Red))
  }

  def setupRepo(nc: Config): Unit = {
    val directory = nc.local.toPath
    println(fansi.Str("Cloning repository...").overlay(fansi.Bold.On ++ fansi.Color.Cyan))
    Setup.gclone(nc.remote, nc.local)
    println(fansi.Str("Emptying file tree and committing default configuration...").overlay(fansi.Color.Cyan))
    Files.walkFileTree(directory, gitVisibleDeletion(directory))

    val default = Selection.default
    val selection = new File(nc.local, "selection.toml")
    val fw = new FileWriter(selection)
    fw.write(default.toml)
    fw.close()

    Setup.gadd(nc.local, ".")
    Setup.gcp(nc.local, "Initial commit from dot")
    println(fansi.Str("Repository all set!").overlay(fansi.Bold.On ++ fansi.Color.Cyan))
  }

  val pureDeletion: SimpleFileVisitor[Path] = new SimpleFileVisitor[Path]() {
    override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
      print(s"\033[2K\rDeleted $file... ")
      Files.delete(file)
      FileVisitResult.CONTINUE
    }

    override def postVisitDirectory(dir: Path, exc: IOException): FileVisitResult = {
      print(s"\033[2K\rDeleted $dir... ")
      Files.delete(dir)
      FileVisitResult.CONTINUE
    }
  }

  def gitVisibleDeletion(root: Path): SimpleFileVisitor[Path] = new SimpleFileVisitor[Path]() {
    override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
      if (!root.relativize(file).toString.contains(".git")) {
        print(s"\033[2K\rDeleted $file... ")
        Files.delete(file)
        new ProcessBuilder("git", "rm", "--cached", "-f", root.relativize(file).toString).directory(root.toFile)
        FileVisitResult.CONTINUE
      } else {
        FileVisitResult.SKIP_SIBLINGS
      }
    }

    override def preVisitDirectory(dir: Path, attrs: BasicFileAttributes): FileVisitResult = {
      if (root.relativize(dir).toString.contains(".git")) {
        FileVisitResult.SKIP_SUBTREE
      } else {
        FileVisitResult.CONTINUE
      }
    }

    override def postVisitDirectory(dir: Path, exc: IOException): FileVisitResult = {
      if (!root.relativize(dir).toString.contains(".git") && dir.toFile.listFiles().isEmpty) {
        print(s"\033[2K\rDeleted $dir... ")
        Files.delete(dir)
      }
      FileVisitResult.CONTINUE
    }
  }

}
