package tf.bug.dot

import java.io._
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes

import fansi.{EscapeAttr, Str}

import scala.annotation.tailrec
import scala.io.{Source, StdIn}

object Setup {

  def gclone(remoteRepository: String, localDirectory: File): Unit = {
    val pb = new ProcessBuilder("git", "clone", remoteRepository, localDirectory.getAbsolutePath)
    pb.start().waitFor()
  }

  def gadd(localDirectory: File, selector: String): Unit = {
    val pb = new ProcessBuilder("git", "add", selector).directory(localDirectory)
    pb.start().waitFor()
  }

  def gcp(localDirectory: File, message: String): Unit = {
    val pb = new ProcessBuilder("git", "commit", "-m", message).directory(localDirectory)
    pb.start().waitFor()
    val pb2 = new ProcessBuilder("git", "push", "-u", "origin", "master").directory(localDirectory)
    pb2.start().waitFor()
  }

  def clean(localDirectory: File): Unit = {
    val d = localDirectory.toPath
    Files.walkFileTree(Paths.get(System.getProperty("user.home")), linkDissolving((d)))
  }

  def linkDissolving(target: Path): SimpleFileVisitor[Path] = new SimpleFileVisitor[Path]() {
    override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
      if (Files.isSymbolicLink(file)) {
        val orig = Files.readSymbolicLink(file)
        if (orig.toAbsolutePath.startsWith(target.toAbsolutePath)) {
          if (Questioning.boolean(s"Dissolve ${file.toAbsolutePath.toString} from ${orig.toAbsolutePath.toString}?")) {
            val s = Source.fromFile(orig.toFile).mkString
            Files.delete(file)
            file.toFile.createNewFile()
            val fw = new FileWriter(file.toFile)
            fw.write(s)
            fw.close()
          }
        }
      }
      FileVisitResult.CONTINUE
    }
  }

  val bold: EscapeAttr = fansi.Bold.On
  val grey: EscapeAttr = fansi.Color.LightGray

  def run(): Config = {
    val welcome = fansi.Str("Welcome!").overlay(bold) ++
      fansi.Str(" It seems you've never used dot on this machine in the past ") ++
      fansi.Str("(or deleted your previous config)").overlay(grey) ++ fansi.Str(".")
    println(welcome)
    val ld = Questioning.validate(
      "Dotfiles local directory",
      "`Documents/dotfiles` for the repo to be placed in `~/Documents/dotfiles`",
      "Documents/dotfiles",
      s => {
        val ldf = new File(System.getProperty("user.home"), s)
        ldf.exists() || ldf.mkdirs()
      },
      "Not a valid directory."
    )
    val ldf = new File(System.getProperty("user.home"), ld)
    val rr = Questioning.validate(
      "Dotfiles remote repository",
      "`https://gitlab.com/example/dotfiles.git` for user `example`'s dotfiles",
      "",
      s => {
        val pb = new ProcessBuilder("git", "ls-remote", "--exit-code", "-h", s).inheritIO()
        pb.start().waitFor() == 0
      },
      "Not a valid repository"
    )
    Config(ldf, rr)
  }

}
