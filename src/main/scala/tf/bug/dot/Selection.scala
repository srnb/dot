package tf.bug.dot

import java.io.{File, FileWriter}

import tf.bug.dot.Selection.implicits._
import tf.bug.dot.tu.AsToml
import tf.bug.dot.tu.AsToml.implicits._
import toml.Codecs._
import toml.Toml

import scala.io.Source

case class Selection(groups: List[Group])

case class Group(name: String, entries: List[Entry])

case class Entry(home: String, repo: String)

object Selection {

  final val default = Toml.parseAs[Selection](BuildInfo.defaultSelectionToml).right.get

  object implicits {

    implicit def selectionAsToml: AsToml[Selection] = new AsToml[Selection] {
      override def asToml(a: Selection): String = s"groups = ${a.groups.toml}"
    }

    implicit def groupAsToml: AsToml[Group] = new AsToml[Group] {
      override def asToml(a: Group): String = s"""name = ${a.name.toml}
                                                 |entries = ${a.entries.toml}""".stripMargin
    }

    implicit def entryAsToml: AsToml[Entry] = new AsToml[Entry] {
      override def asToml(a: Entry): String = s"""home = ${a.home.toml}
                                                 |repo = ${a.repo.toml}""".stripMargin
    }

  }

  def apply(): Selection = {
    val f = new File(Config().local, "selection.toml")
    val c = Source.fromFile(f).mkString
    Toml.parseAs[Selection](c) match {
      case Left((a, m)) => throw new IllegalStateException(s"Error when parsing selection file! $m ${a.mkString(":")}")
      case Right(nc) => nc
    }
  }

  def set(s: Selection): Unit = {
    val t = s.toml
    val fw = new FileWriter(new File(Config().local, "selection.toml"))
    fw.write(t)
    fw.close()
  }

}
