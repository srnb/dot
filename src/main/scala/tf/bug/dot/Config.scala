package tf.bug.dot

import java.io.File

import tf.bug.dot.Config.implicits._
import tf.bug.dot.tu.AsToml
import tf.bug.dot.tu.AsToml.implicits._
import toml.Codec.{Address, Message}
import toml.{Codec, Toml, Value}
import toml.Codecs._

import scala.io.Source

case class Config(local: File, remote: String)

object Config {

  def apply(): Config = {
    val f = new File(System.getProperty("user.home"), ".dot/config.toml")
    val c = Source.fromFile(f).mkString
    Toml.parseAs[Config](c) match {
      case Left((a, m)) => throw new IllegalStateException(s"Error when parsing configuration! $m ${a.mkString(":")}")
      case Right(nc) => nc
    }
  }

  object implicits {

    implicit def configAsToml: AsToml[Config] = new AsToml[Config] {
      override def asToml(a: Config): String =
        s"""local = ${a.local.getAbsolutePath.toml}
           |remote = ${a.remote.toml}""".stripMargin
    }

    implicit def fileCodec: Codec[File] = new Codec[File] {
      override def apply(value: Value, defaults: Map[String, Any]): Either[(Address, Message), File] = {
        value match {
          case Value.Str(s) => Right(new File(s))
        }
      }
    }

  }

}
