import scala.io.Source

lazy val root = (project in file(".")).settings(
  name := "dot",
  organization := "tf.bug",
  version := "0.2.0",
  scalaVersion := "2.11.12",
  libraryDependencies ++= Seq(
    "com.github.scopt" %%% "scopt" % "3.7.0",
    "tech.sparse" %%% "toml-scala" % "0.1.1",
    "com.lihaoyi" %%% "fansi" % "0.2.5"
  ),
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion, BuildInfoKey.action("defaultSelectionToml") {
    Source.fromFile(baseDirectory.value / "default_selection.toml").mkString
  }),
  buildInfoPackage := "tf.bug.dot"
).enablePlugins(ScalaNativePlugin, BuildInfoPlugin)